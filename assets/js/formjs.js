/**
 * @filesource : formjs.js
 * @author : Shabeeb  <me@shabeebk.com>
 * @abstract : controller fo HTML page
 * @package sample file 
 * @copyright (c) 2014, Shabeeb
 * shabeebk.com
 * 
 *  */
var app = angular.module('formExample', []);

app.controller("formCtrl", ['$scope', '$http', function($scope, $http) {

    $scope.url = 'php/insert_customer_credentials.php';

    $scope.formsubmit = function(isValid) {


        if (isValid) {
            

            $http.post($scope.url, {"name": $scope.name, "number": $scope.number, "email": $scope.email, "message": $scope.message})
                .success(function(data, status) {
                    console.log(data);
                    $scope.status = status;
                    $scope.data = data;
                    $scope.result = data; // Show result from server in our <pre></pre> element
                    swal( $scope.name, "Your message to IFGI has been sent! Thanks.", "success");
                })
                .error(function(data, status) {
                    swal( "FAILED!", "Message sending failed! Kindly check your internet connection", "error");
                })
        }else{
            
                swal( "INVALID!", "Your entry is not valid! Please input correctly", "warning");
        }


    }

}]);

var app = angular.module('adminFormExample', []);

app.controller("adminFormCtrl", ['$scope', '$http', function($scope, $http) {

    $scope.url = 'php/check_admin_credentials.php';

    $scope.formsubmit = function(isValid) {


        if (isValid) {
            

            $http.post($scope.url, {"username": $scope.username, "password": $scope.password})
                .success(function(data, status) {
                    console.log(data);
                    if( data == 'You are authorized!'){
                        window.location = "php/admin.php";
                    } else {
                        swal( "FAILED!", "You are unauthorized!", "error");
                    }
                })
                .error(function(data, status) {
                    swal( "FAILED!", "Kindly check your internet connection", "error");
                })
        }else{
            
                swal( "INVALID!", "Your entry is not valid! Please input correctly", "warning");
        }


    }

}]);