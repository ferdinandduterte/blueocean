<?php

    require_once 'connect.php';

    $post_data = file_get_contents("php://input");
    $data = json_decode($post_data);

    $sql = "SELECT * FROM admin_account WHERE id = 1" ;
    
    $result =  mysqli_fetch_array( mysqli_query($connect, $sql) );
    
    if( ($result["username"] == $data->username) && ($result["password"] == $data->password) ) {
        echo 'You are authorized!';
    } else{
        echo 'You are unauthorized!';
    }
?>