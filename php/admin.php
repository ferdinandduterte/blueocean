<!DOCTYPE html>
<html lang="en" >

  <head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
    
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <style>
      h2:before{
        opacity: 0;
      }
    </style>
  </head>

  <body>

    <main>
      <div class="title">
        <img src="../assets/img/logo.png" alt="IFGI">
        <h2>ADMIN</h2>
        <a href="javascript:void(0);">Hello Boss !</a>
      </div>

      <article class="larg">

        <table id="table_id" class="display">
          <thead>
              <tr>
                  <th>Date Sent</th>
                  <th>Name</th>
                  <th>Contact Number</th>
                  <th>Email</th>
                  <th>Message</th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th>Date Sent</th>
                  <th>Name</th>
                  <th>Contact Number</th>
                  <th>Email</th>
                  <th>Message</th>
              </tr>
          </tfoot>
          <tbody>      
          <?php
              require_once 'connect.php';  
              $tbody_content = '';
              $sql = "SELECT * FROM client ORDER BY sent DESC;";  //sql to that database
              $result = mysqli_query($connect, $sql);

              if(mysqli_num_rows($result) > 0)  //checking rows
              {
                  while($row = mysqli_fetch_array($result))
                  {
                      $tbody_content .= '
                      <tr>
                          <td style="text-align:center;">'.$row["sent"].'</td>
                          <td style="text-align:center;">'.$row["name"].'</td>
                          <td style="text-align:center;">'.$row["number"].'</td>
                          <td style="text-align:center;">'.$row["email"].'</td>
                          <td style="text-align:center;">'.$row["message"].'</td>
                      </tr>
                      ';
                  }

              }
              else
              {
                  $tbody_content .= '
                      <tr>
                          <td colspan="5" style="text-align:center;">
                              Client Data Not Found!!!
                          </td>
                      </tr>';
              }

              echo $tbody_content;
          ?>
          </tbody>
        </table>
      </article>
    </main>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script  src="js/index.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
      $(document).ready( function () {
          $('#table_id').DataTable({
            "order": [[ 0, 'desc' ]]
          });

      });
    </script>
  </body>
</html>
